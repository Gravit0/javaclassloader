/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author gravit
 */
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PluginLoader extends ClassLoader {
    private Map<String, Class<?>> cacheClass = new HashMap<>();
    private ClassLoader parent;

    public PluginLoader(ClassLoader parent) {
        this.parent = parent;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        Class<?> result = cacheClass.get(name);

        if (result == null) {
            try {
                result = parent.loadClass(name);
            } catch (ClassNotFoundException classNotFoundException) {
            }
        }

        return result;
    }
    
    public void loadPlugin(String strPluginFile) throws IOException {
        JarFile jarFile = new JarFile(strPluginFile);
        System.out.println("Started");
        Enumeration<JarEntry> jarEntries = jarFile.entries();
        
        while (jarEntries.hasMoreElements()) {
            JarEntry jarEntry = jarEntries.nextElement();
            
            if (jarEntry.isDirectory())
                continue;

            if (jarEntry.getName().endsWith(".class")) {
                try {
                    byte[] classData = loadClassData(jarFile, jarEntry);
                    if (classData != null) {
                        Class<?> clazz = null;
                        try {
                            System.out.println(jarEntry.getName());
                                clazz = defineClass(
                                        jarEntry.getName().replace('/', '.').substring(0, jarEntry.getName().length() - 6),
                                        classData, 0, classData.length);
                            } catch (NoClassDefFoundError classFormatError) {
                                System.out.print("[NoClassDefFoundError] ");
                                //System.out.print(classFormatError.getMessage());
                            }catch (ClassFormatError classFormatError) {
                                System.out.print("[ClassFormatError] ");
                        }
                        try {
                            if (clazz != null) {
                                System.out.println(clazz.getName());
                            }
                            else System.out.println("[null]");
                        } catch (NoClassDefFoundError e) {
                            System.out.print("[NoClassDefFoundError] ");
                        }
                        cacheClass.put(clazz.getName(), clazz);
                    }
                } catch (Exception exception) {
                    
                    //System.out.print(exception.toString());
                }
            }
        }
    }

    public void loadPlugins() throws IOException {
        // получаем список jar-файлов

        String[] jarList = new File("plugins/").list(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.toLowerCase().endsWith(".jar");
            }
        });

        // загружаем каждый плагин

        for (String strJarFile : jarList) {
            loadPlugin(strJarFile);
        }
    }

    // получаем байт-код класса

    private byte[] loadClassData(JarFile jarFile, JarEntry jarEntry)
            throws IOException {
        long size = jarEntry.getSize();
        if (size <= 0)
            return null;
        else if (size > Integer.MAX_VALUE) {
            throw new IOException("Class size too long");
        }

        byte[] buffer = new byte[(int) size];
        InputStream is = jarFile.getInputStream(jarEntry);
        is.read(buffer);

        return buffer;
    }
}