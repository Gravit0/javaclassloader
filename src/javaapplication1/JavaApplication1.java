/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;
import java.util.Scanner;

/**
 *
 * @author gravit
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     * @throws java.lang.ClassNotFoundException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.NoSuchMethodException
     * @throws java.lang.InstantiationException
     * @throws java.lang.reflect.InvocationTargetException
     */
    public static void main(String[] args) throws IllegalArgumentException, MalformedURLException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, Throwable {
        // TODO code application logic here
        FileInputStream file = null;
        try {
            file = new FileInputStream("./loaderconfig.ini");
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println("Not found loaderconfig.ini");
            return;
        }
        Properties props = new Properties();
	props.load(file);
        String filename = props.getProperty("filename");
        String classname = props.getProperty("class");
        if(classname==null) classname="Mainclass";
        String methodname = props.getProperty("method");
        if(methodname==null) methodname="main";
        System.out.println(filename);
        System.out.println("START");
        URL[] classLoaderUrls = new URL[]{new URL(filename)};
        
        URLClassLoader r = new URLClassLoader(classLoaderUrls);
        Class<?> beanClass = r.loadClass(classname);
        /*
        System.out.println("Started");
        Enumeration<JarEntry> jarEntries = jarFile.entries();
        ArrayList<Class> clases = new ArrayList();
        while (jarEntries.hasMoreElements()) {
            JarEntry jarEntry = jarEntries.nextElement();
            
            if (jarEntry.isDirectory())
                continue;

            if (jarEntry.getName().endsWith(".class")) {
                String substring = jarEntry.getName().replace('/', '.').substring(0, jarEntry.getName().length() -6);
                Class beanClass2 = (Class) r.loadClass(substring);
                clases.add(beanClass2);
            }*/
        /*PluginLoader r = new PluginLoader(ClassLoader.getSystemClassLoader());
        r.loadPlugin("/home/gravit/.grand-mine/launcher.jar");
        Class beanClass = (Class) r.loadClass("Mainclass");*/
        Constructor<?> constructor = beanClass.getConstructor();
        Object beanObj = constructor.newInstance();
        Method[] methods = beanObj.getClass().getMethods();
        Class[] paramTypes = new Class[] { String[].class };
        Method method = beanClass.getMethod(methodname,paramTypes);
        Thread myThready = new Thread(new Runnable()
		{
                        @Override
			public void run() //Этот метод будет выполняться в побочном потоке
			{
				Scanner sc = new Scanner(System.in);
                                while(true)
                                {
                                    try {
                                        String string = sc.nextLine();
                                        if(string.equals("gc"))
                                        {
                                    
                                            System.gc();
                                            System.out.println("[CHAT] GC OK");
                                    
                                        }
                                    } catch (Exception e) {
                                        System.err.println(e.getMessage());
                                    }
                                }
			}
		});
		myThready.start();
        Object[] res = {(Object) args};
        try {
            method.invoke(beanObj, res);
        } catch (IllegalAccessException | IllegalArgumentException illegalAccessException) {
        } catch (InvocationTargetException invocationTargetException) {
            Throwable targetException = invocationTargetException.getTargetException();
            throw targetException;
        }
        System.out.println("THE END"); 
        }
        /*
        Class plugin = (Class) loader.loadClass("Mainclass");
        Constructor[] constructors = plugin.getConstructors(); 
for (Constructor constructor : constructors) { 
    Class[] paramTypes = constructor.getParameterTypes();
    for (Class paramType : paramTypes) { 
        System.out.print(paramType.getName() + " "); 
    } 
    System.out.println(); 
} 
        Method[] list = plugin.getMethods();
        for(int i=0;i<list.length;++i)
        {
            Method li= list[i];
            System.out.println("Имя: " + li.getName()); 
    System.out.println("Возвращаемый тип: " + li.getReturnType().getName()); 
 
    Class[] paramTypes = li.getParameterTypes(); 
    System.out.print("Типы параметров: "); 
    for (Class paramType : paramTypes) { 
        System.out.print(" " + paramType.getName()); 
    } 
    System.out.println(); 
            System.out.write(li.getName().getBytes());
            System.out.write('\n');
            if(li.getName().equals("main"))
            {
                Object[] r = {(Object) null};
                try {
                    li.invoke(null, r);
                } catch (IllegalAccessException illegalAccessException) {
                } catch (IllegalArgumentException illegalArgumentException) {
                } catch (InvocationTargetException invocationTargetException) {
                    System.out.print(Arrays.toString(invocationTargetException.getCause().getStackTrace()));
                }
            }
        }*/
    }
